# 1、写一个面向对象的例子：
# - 比如创建一个类（Animal）【动物类】，类里有属性（名称，颜色，年龄，性别），类方法（会叫，会跑）
# - 创建子类【猫】，继承【动物类】
# - 重写父类的__init__方法，继承父类的属性
# - 添加一个新的属性，毛发=短毛
# - 添加一个新的方法， 会捉老鼠，
# - 重写父类的【会叫】的方法，改成【喵喵叫】
# - 创建子类【狗】，继承【动物类】
# - 复写父类的__init__方法，继承父类的属性
# - 添加一个新的属性，毛发=长毛
# - 添加一个新的方法， 会看家
# - 复写父类的【会叫】的方法，改成【汪汪叫】
# 2、在入口函数中创建类的实例
# - 创建一个猫猫实例
# - 调用捉老鼠的方法
# - 打印【猫猫的姓名，颜色，年龄，性别，毛发，捉到了老鼠】
# - 创建一个狗狗实例
# - 调用【会看家】的方法
# - 打印【狗狗的姓名，颜色，年龄，性别，毛发】

class animal:
    name = 1
    color =1
    age =1
    sex =1

    def __init__(self,name,color,age,sex):
        self.name = name
        self.color = color
        self.age = age
        self.sex = sex

    def shout(self):
        print("会叫")

    def run(self):
        print("会跑")

class cat(animal):
    hair = '短毛'

    def __init__(self,name,color,age,sex,hair):
        self.name = name
        self.color = color
        self.age = age
        self.sex = sex
        self.hair = '短毛'

    @classmethod
    def catch(self):
        print("会抓老鼠")

    def shout(self):
        print("喵喵叫")

class dog(animal):
    hair = '长毛'

    def __init__(self,name,color,age,sex,hair):
        self.name = name
        self.color = color
        self.age = age
        self.sex = sex
        self.hair = '长毛'

    @classmethod
    def home(self):
        print("会看家")

    def shout(self):
        print("汪汪叫")

if __name__ == '__main__':

    xiaoman = cat('xiaoman','blue','1.5','boy','短毛')
    print(f'猫猫的名字是：{xiaoman.name},颜色是：{xiaoman.color},年龄是：{xiaoman.age},毛发是：{xiaoman.hair},性别是：{xiaoman.sex}')
    cat.catch()

    duanwu = cat('duanwu','white','0.5','boy','长毛')
    print(f'狗狗的名字是：{duanwu.name},颜色是：{duanwu.color},年龄是：{duanwu.age},毛发是：{duanwu.hair},性别是：{duanwu.sex}')
    dog.home()


